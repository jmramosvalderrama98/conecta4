package models;

import controllers.Logic;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import types.Color;
import views.BoardView;
import views.View;

import static org.junit.jupiter.api.Assertions.*;

class GameTest {

    Game game;

    @org.junit.jupiter.api.BeforeEach
    void setUp() {
        this.game = new Game();
        game.introduceCard(0, Color.O);
        game.introduceCard(0,Color.O);
        game.introduceCard(4,Color.O);
        game.introduceCard(4,Color.O);
        game.introduceCard(4,Color.O);
        game.introduceCard(2,Color.O);
        game.introduceCard(3,Color.O);

        game.introduceCard(0,Color.X);
        game.introduceCard(2,Color.X);
        game.introduceCard(1,Color.X);
        game.introduceCard(1,Color.X);
        game.introduceCard(3,Color.X);
        game.introduceCard(3,Color.X);
        game.introduceCard(3,Color.X);

        Logic logic = new Logic(game);
        BoardView boardView = new BoardView(logic);
        boardView.printBoard();
    }

    @org.junit.jupiter.api.Test
    void oWinnerVertical(){
        game.nextTurn();
        assert(!game.playerInTurnIsWinner());
        game.introduceCard(4,Color.O);
        assert(game.playerInTurnIsWinner());
    }

    @org.junit.jupiter.api.Test
    void owinnerHorizontal(){
        game.nextTurn();
        assert(!game.playerInTurnIsWinner());
        game.introduceCard(5,Color.O);
        game.introduceCard(6,Color.O);
        assert(game.playerInTurnIsWinner());
    }

    @org.junit.jupiter.api.Test
    void xwinnerVertical(){
        assert(!game.playerInTurnIsWinner());
        game.introduceCard(3,Color.X);
        assert(game.playerInTurnIsWinner());
    }

    @org.junit.jupiter.api.Test
    void xwinnerDiagonal(){
        assert(!game.playerInTurnIsWinner());
        game.introduceCard(4,Color.X);
        assert(game.playerInTurnIsWinner());
    }


}