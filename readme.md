# Conecta 4

En este código se ha desarrollado la lógica del juego Conecta 4. 

En un primer lugar se desarrolló sin tener en cuenta ninguna separación en módulos. A partir de ahí,
se comenzaron a realizar algunas mejoras para delegar responsabilidades para tratar de disminuir el 
acoplamiento y aumentar la cohesión. Posteriormente, se realizó una separación por vistas y modelos
de tal forma que los cambios que se pudieran relizar en un módulo no afectaran al otro. Por último, se terminó
separando por modelos, vistas y controladores completando el patrón MVC.

Las diferentes versiones del código con la evolución del mismo se pueden observar en el siguiente repositorio:
https://bitbucket.org/jmramosvalderrama98/conecta4