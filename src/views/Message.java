package views;

public enum Message {
    START("This is Tic Tac Toe"),
    USER_PLAY("models.Turn of #player"),
    WIN_RESULT("The game is finished. Congratulations to #player"),
    DRAW_RESULT("The game is finished. There is no winner");

    private String message;
    private Message(String message){
        this.message = message;
    }

    public void replaceMessage(String player){
        this.message.replaceAll("#player", player);
    }

    @Override
    public String toString(){
        return this.message;
    }

}
