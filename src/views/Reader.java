package views;

import java.util.Scanner;

public class Reader {
    private static Scanner sc = new Scanner(System.in);

    public static int getUserSquareInput(){
        int userInput;
        do {
            userInput = sc.nextInt();
        }while(userInput < 0 || userInput > 7);
        return  userInput;
    }
}
