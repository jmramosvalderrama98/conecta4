package views;

import controllers.Logic;
import types.Color;

public class PlayerView extends ViewWithLogic{

    public PlayerView(Logic logic){
        super(logic);
    }

    public void interact(){
        int userInput;
        do {
            assert(logic.getColorInTurn() != Color.NULL);
            Printer.getInstance().writeMessage(Message.USER_PLAY, logic.getColorInTurn().name());
            userInput = Reader.getUserSquareInput();
        }while(logic.columnisFull(userInput));
        logic.introduceCard(userInput);
    }
}
