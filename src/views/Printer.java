package views;


public class Printer {

    private static Printer printer;
    private Printer(){
    }

    public static Printer getInstance(){
        if(printer == null){
            printer = new Printer();
        }
        return printer;
    }

   public void writeln(){
        System.out.print('\n');
   }

   public void write(String s){
        System.out.print(s);
   }

    public void writeMessage(Message message){
        System.out.println(message);
    }

    public void writeMessage(Message message, String replaceString){
        message.replaceMessage(replaceString);
        System.out.println(message);
    }

}
