package views;

import controllers.Logic;

public class View extends ViewWithLogic{
    private PlayView playView;
    private StartView startView;
    private EndView endView;

    public View(Logic logic){
        super(logic);
        this.playView = new PlayView(this.logic);
        startView = new StartView();
        endView = new EndView(this.logic);
    }

    public void start(){
        startView.start();
    }

    public void play(){
        this.playView.play();
    }

    public void end(){
        endView.end();
    }

}
