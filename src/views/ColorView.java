package views;

import types.Color;

public class ColorView {
    public void printColor(Color color){
        Printer.getInstance().write(color != Color.NULL ? color.name() : ".");
    }
}
