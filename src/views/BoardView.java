package views;

import controllers.Logic;

public class BoardView extends ViewWithLogic{

    public BoardView(Logic logic){
        super(logic);
    }

    public void printBoard(){
        final int BOARD_DIMENSION = logic.getBoardDimension();
        for(int i = BOARD_DIMENSION - 1; i >= 0; i--){
            for(int j=0; j< BOARD_DIMENSION; j++){
                new ColorView().printColor(logic.getSquareColorByPosition(i,j));
            }
            Printer.getInstance().writeln();
        }
    }
}
