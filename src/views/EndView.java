package views;

import controllers.Logic;
import types.Color;

public class EndView extends ViewWithLogic{

    public EndView(Logic logic){
        super(logic);
    }

    public void end(){
        if(!logic.playerInTurnIsWinner()){
            Printer.getInstance().writeMessage(Message.DRAW_RESULT);
        }
        else{
            assert(logic.getColorInTurn() != Color.NULL);
            Printer.getInstance().writeMessage(Message.WIN_RESULT, logic.getColorInTurn().name());
        }
    }
}
