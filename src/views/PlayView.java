package views;

import controllers.Logic;

public class PlayView extends ViewWithLogic{
    private BoardView boardView;
    private PlayerView playerView;

    public PlayView(Logic logic){
        super(logic);
        boardView = new BoardView(logic);
        playerView = new PlayerView(logic);
    }

    public void play(){
        do{
            boardView.printBoard();
            playerView.interact();
            logic.nextTurn();
        }while(!logic.playerInTurnIsWinner() && !logic.boardisFull());
    }
}
