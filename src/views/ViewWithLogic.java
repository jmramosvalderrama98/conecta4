package views;

import controllers.Logic;

public abstract class ViewWithLogic {
    protected Logic logic;

    public ViewWithLogic(Logic logic){
        this.logic = logic;
    }
}
