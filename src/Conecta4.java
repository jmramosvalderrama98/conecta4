import controllers.Logic;
import models.Game;
import views.View;

public class Conecta4 {
    private Game game;
    private View view;
    private Logic logic;

    public Conecta4(){
        this.game = new Game();
        this.logic = new Logic(this.game);
        this.view = new View(this.logic);
    }

    private void play(){
        view.start();
        view.play();
        view.end();
    }

    public static void main(String[] args){
        new Conecta4().play();
    }
}
