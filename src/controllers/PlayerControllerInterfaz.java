package controllers;

import types.Color;

public interface PlayerControllerInterfaz {

    boolean playerInTurnIsWinner();

    Color getColorInTurn();

    void introduceCard(int userInput);

}
