package controllers;

import types.Color;
import models.Game;

public class PlayerController extends Controller implements PlayerControllerInterfaz {

    public PlayerController(Game game){
        super(game);
    }

    public boolean playerInTurnIsWinner() {
        return game.playerInTurnIsWinner();
    }

    public Color getColorInTurn() {
        return game.getColorInTurn();
    }

    public void introduceCard(int userInput) {
        game.introduceCard(userInput, this.getColorInTurn());
    }

}
