package controllers;

import types.Color;
import models.Game;

public class BoardController extends Controller implements BoardControllerInterfaz {

    public BoardController(Game game) {
        super(game);
    }

    public int getBoardDimension() {
        return game.getBoardDimension();
    }

    public Color getSquareColorByPosition(int i, int j) {
        return this.game.getSquareColorByPosition(i,j);
    }

    public boolean columnisFull(int column) {
        return game.columnisFull(column);
    }
}
