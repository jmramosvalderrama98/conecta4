package controllers;

import types.Color;

public interface BoardControllerInterfaz {

    int getBoardDimension();

    Color getSquareColorByPosition(int i, int j);

    boolean columnisFull(int column);
}
