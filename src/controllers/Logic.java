package controllers;

import types.Color;
import models.Game;

public class Logic implements BoardControllerInterfaz, PlayerControllerInterfaz {
    private Game game;
    private PlayerControllerInterfaz playerController;
    private BoardControllerInterfaz boardController;

    public Logic(Game game){
        this.game = game;
        this.playerController = new PlayerController(this.game);
        this.boardController = new BoardController(this.game);
    }

    public void nextTurn() {
        game.nextTurn();
    }

    public boolean boardisFull() {
        return this.game.boardisFull();
    }

    public boolean playerInTurnIsWinner() {
        return this.playerController.playerInTurnIsWinner();
    }

    public int getBoardDimension() {
        return boardController.getBoardDimension();
    }

    public Color getSquareColorByPosition(int i, int j) {
        return boardController.getSquareColorByPosition(i,j);
    }

    public boolean columnisFull(int userInput) {
        return boardController.columnisFull(userInput);
    }

    public void introduceCard(int userInput) {
        playerController.introduceCard(userInput);
    }

    public Color getColorInTurn() {
        return playerController.getColorInTurn();
    }
}
