package models;

import types.Color;

public class Turn {
    private Player[] players;
    private int indexPlayerInTurn;
    private Board board;

    public static int numberofPlayers = 2;

    public Turn(Board board){
        this.players = new Player[numberofPlayers];
        this.board = board;

        for(int i=0; i<numberofPlayers; i++){
            this.players[i] = new Player(Color.get(i), board);
        }
        indexPlayerInTurn = 0;
    }

    public Color getColor(){
        return players[indexPlayerInTurn].getColor();
    }

    public void nextTurn(){
        if(!this.playerInTurnIsWinner()){
            this.indexPlayerInTurn = (this.indexPlayerInTurn + 1) % this.numberofPlayers;
        }
    }

    public boolean playerInTurnIsWinner(){
        return this.players[indexPlayerInTurn].isWinner();
    }
}
