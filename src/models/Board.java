package models;

import types.Color;

public class Board {
    private Color[][] squares;

    public final int BOARD_DIMENSION = 7;

    public Board(){
        squares = new Color[BOARD_DIMENSION][BOARD_DIMENSION];

        for(int i=0; i<BOARD_DIMENSION; i++){
            for(int j=0; j<BOARD_DIMENSION; j++){
                squares[i][j] = Color.NULL;
            }
        }
    }

    public boolean squareIsBlank(int i, int j){
        return squares[i][j].isNull();
    }

    public Color getSquareColorByPosition(int i, int j){
        return squares[i][j];
    }

    public void introduceCard(int column, Color playerColor){
        assert(!columnisFull(column));

        int i = BOARD_DIMENSION - 1;
        while(i >= 0 && squares[i][column].isNull()){
                i--;
        }
        squares[i+1][column] = playerColor;
    }

    public boolean columnisFull(int column){
        assert(column < BOARD_DIMENSION);
        for(int i=0; i < BOARD_DIMENSION; i++){
            if(squares[i][column].isNull()){
                return false;
            }
        }
        return true;
    }

    public boolean boardisFull(){
        for(int i=0; i< BOARD_DIMENSION; i++){
            if(!columnisFull(i)){
                return false;
            }
        }
        return true;
    }
}
