package models;

public class HorizontalChecker extends CombinationChecker{

    public HorizontalChecker(Board board, Player player){
        super(board, player);
    }

    @Override
    public boolean hasWinnerCombination() {
        return ocurrencesCalculator.checkGameOcurrences(new Coordenate(0,0), new Coordenate(1,0), new Coordenate(0,1));
    }
}
