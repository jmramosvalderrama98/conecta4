package models;

import types.Color;

public class Player {
    private Color color;
    private Board board;

    public Player(Color color, Board board){
        this.color = color;
        this.board = board;
    }

    public Color getColor() {
        return color;
    }

    public boolean isWinner(){
        CombinationChecker verticalChecker = new VerticalChecker(board, this);
        CombinationChecker horizontalChecker = new HorizontalChecker(board, this);
        CombinationChecker diagonalChecker = new DiagonalChecker(board, this);

        return verticalChecker.hasWinnerCombination() ||
                horizontalChecker.hasWinnerCombination() ||
                diagonalChecker.hasWinnerCombination();
    }
}
