package models;

public class DiagonalChecker extends CombinationChecker{

    public DiagonalChecker(Board board, Player player){
        super(board, player);
    }

    @Override
    public boolean hasWinnerCombination() {
        return ocurrencesCalculator.checkGameOcurrences(new Coordenate(0, super.board.BOARD_DIMENSION - 1), new Coordenate(1,1), new Coordenate(0, -1)) ||
            ocurrencesCalculator.checkGameOcurrences(new Coordenate(0,0), new Coordenate(-1,1), new Coordenate(1,1)) ||
            ocurrencesCalculator.checkGameOcurrences(new Coordenate(super.board.BOARD_DIMENSION - 1,0), new Coordenate(1,1), new Coordenate(-1,0)) ||
            ocurrencesCalculator.checkGameOcurrences(new Coordenate(super.board.BOARD_DIMENSION - 1,super.board.BOARD_DIMENSION - 1),
                new Coordenate(-1,1), new Coordenate(0, -1));
    }
}
