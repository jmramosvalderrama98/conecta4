package models;

public class OcurrencesDetector {
    private Board board;
    private Player player;

    public static final int numOcurrences = 4;

    public OcurrencesDetector(Board board, Player player){
        this.board = board;
        this.player = player;
    }

    public boolean checkGameOcurrences(Coordenate beginning, Coordenate ocurrencesMovement, Coordenate loopMovement) {
        boolean ocurrencesCheck = false;
        int jLoop = beginning.getX();
        int iLoop = beginning.getY();
        while(!ocurrencesCheck && isInsideBoard(iLoop,jLoop)){
            ocurrencesCheck = iterationIsConecta4(new Coordenate(jLoop,iLoop),ocurrencesMovement);
            jLoop += loopMovement.getX();
            iLoop += loopMovement.getY();
        }
        return ocurrencesCheck;
    }

    private boolean iterationIsConecta4(Coordenate startPostition, Coordenate ocurrencesMovement){
        int ocurrencesCount = 0;
        int i = startPostition.getY();
        int j = startPostition.getX();
        boolean ocurrencesCheck = false;
        while(isInsideBoard(i,j) && !ocurrencesCheck){
            if(!squareIsOwnedByPlayer(new Coordenate(j,i))){
                ocurrencesCount = 0;
            }
            else{
                ocurrencesCount++;
            }
            if(ocurrencesCount >= numOcurrences){
                ocurrencesCheck = true;
            }
            j += ocurrencesMovement.getX();
            i += ocurrencesMovement.getY();
        }
        return ocurrencesCheck;
    }

    private boolean squareIsOwnedByPlayer(Coordenate coordenate){
        return !board.squareIsBlank(coordenate.getY(), coordenate.getX()) && board.getSquareColorByPosition(coordenate.getY(), coordenate.getX()).equals(player.getColor());
    }

    private boolean isInsideBoard(int i, int j){
        return isInsideBoard(i) && isInsideBoard(j);
    }

    private boolean isInsideBoard(int coordenateItem){
        return coordenateItem >= 0 && coordenateItem < board.BOARD_DIMENSION;
    }
}
