package models;

import types.Color;

public class Game {
    private Board board;
    private Turn turn;


    public Game() {
        this.board = new Board();
        this.turn = new Turn(board);
    }


    public int getBoardDimension(){
        return this.board.BOARD_DIMENSION;
    }

    public void nextTurn(){
        turn.nextTurn();
    }

    public boolean playerInTurnIsWinner(){
        return turn.playerInTurnIsWinner();
    }

    public boolean boardisFull(){
        return board.boardisFull();
    }

    public boolean columnisFull(int column){
        return board.columnisFull(column);
    }

    public Color getColorInTurn(){
        return turn.getColor();
    }

    public void introduceCard(int column, Color color){
        this.board.introduceCard(column,color);
    }

    public Color getSquareColorByPosition(int i, int j) {
        return this.board.getSquareColorByPosition(i,j);
    }
}
