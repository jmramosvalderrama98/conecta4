package models;

public abstract class CombinationChecker {
    protected OcurrencesDetector ocurrencesCalculator;
    protected Board board;
    public CombinationChecker(Board board, Player player){
        this.board = board;
        ocurrencesCalculator = new OcurrencesDetector(board, player);
    }

    public abstract boolean hasWinnerCombination();
}
