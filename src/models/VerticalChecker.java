package models;

public class VerticalChecker extends CombinationChecker{

    public VerticalChecker(Board board, Player player){
        super(board, player);
    }

    @Override
    public boolean hasWinnerCombination() {
        return ocurrencesCalculator.checkGameOcurrences(new Coordenate(0,0), new Coordenate(0,1), new Coordenate(1,0));
    }
}
